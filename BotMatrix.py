import time
import requests, re
from slack_bolt import App
from slack_bolt.adapter.fastapi import SlackRequestHandler
from fastapi import FastAPI, Request

api = FastAPI()
app = App()


def get_hilo(payload):
    ts = None
    if 'thread_ts' in payload:
        ts = payload['thread_ts']
    elif 'ts' in payload:
        ts = payload=['ts']
    elif 'message_ts' in payload:
        ts = payload['message_ts']
    
    return ts

def get_service_from_slack(payload):

    servicio = None
    if payload['type'] == 'link_shared':
        url_recibida = payload['links'][0]['url'].replace('http://','').replace('https://','').split('.')
        servicio = url_recibida[0]
    else:
        coincidences = re.findall('\d{5}', payload['text'])
        if len(coincidences) == 1:
            servicio = coincidences[0]
        else:
            coincidences = re.findall('\d{4}', payload['text'])
            if len(coincidences) > 0:
                servicio = coincidences[0]
    return servicio


def get_service_from_matrix(servicio):
    headers = {
        'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    }
    url = f'https://optic.matrix2.cl/api/v1/services-filters/'
    data_consultar = {
        "filters": [['number', 'equal', servicio]],
        "includes": [],
        "excludes": [],
        "unpaid_ballot": False,
        "criterion": 'D'
    }    
    response = requests.get(url=url,headers=headers,verify=False,json=data_consultar)
    return response


def response_data_service(payload, say):

    ts = get_hilo(payload)
    servicio = get_service_from_slack(payload)
    if not servicio:
      say(text='Creo que no me pediste un número de servicio 🐒', thread_ts=ts)
      return None


    response = get_service_from_matrix(servicio)

    if response.status_code == 200:
        if len(response.json()) == 0:
            say(text='¿Seguro existe ese servicio? 🐒', thread_ts=ts) 
            return None 

        response = response.json()[0]
        data_servicio = {
              "text": "Información del servicio",
              "color": "#2eb897",
              "fields": [
                  {
                      "title": "Dirección",
                      "value": response['service__composite_address'],
                      "short": True
                  },
                  {
                      "title": "Comuna",
                      "value": response['service__commune'],
                      "short": True
                  },
                  {
                      "title": "Estado servicio",
                      "value": response['service__status'],
                      "short": True
                  },
                  {
                      "title": "Plan",
                      "value": response['plan__name'],
                      "short": True
                  },                  
              ]
        }

        data_cliente = {
              "text": "Información del cliente",
              "color": "#2eb886",
              "author_name": "Matrix",
              "author_link": "https://optic.matrix2.cl",
              #"title": "Slack API Documentation",
              "fields": [
                  {
                      "title": "Nombre",
                      "value": response['customer__name'],
                      "short": True
                  },
                  {
                      "title": "RUT",
                      "value": response['customer__rut'],
                      "short": True
                  },
                  {
                      "title": "Número",
                      "value": response['customer__phone'],
                      "short": True
                  },
                  {
                      "title": "Correo",
                      "value": response['customer__email'],
                      "short": True
                  },     
              ]
        }

        
        link_matrix = {
              "color": "#2eb886",
              "fields": [
                  {
                      "title": "Ver en matrix",
                      "value": f"<https://optic.matrix2.cl/services/{servicio}/>",
                      "short": False
                  }     
              ],
              "footer": "Matrix Web Service",
              "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
        }
        say(text='Datos del servicio', thread_ts=ts, attachments=[data_cliente, data_servicio, link_matrix]
        )
    else:
        say(text='¿Seguro existe ese servicio?', thread_ts=ts)   


@app.event("link_shared")
def handle_link_shared(payload, say):
    response_data_service(payload, say)

@app.event("app_mention")
def handle_app_mentions(say, payload):
    response_data_service(payload, say)


@api.post("/slack_matrix/")
async def endpoint(req: Request):
    app_handler = SlackRequestHandler(app)
    return await app_handler.handle(req)


@app.message("hola")
def handle_message(message, say, body: dict, client, context):

    message_ts = body["event"]["ts"]
    api_response = client.reactions_add(
        channel=context.channel_id,
        timestamp=message_ts,
        name="white_check_mark",
    )
    say(
        blocks=[
            {
                "type": "section",
                "text": {"type": "mrkdwn", "text": f"Hey there <@{message['user']}>!"},
                "accessory": {
                    "type": "button",
                    "text": {"type": "plain_text", "text": "Click Me"},
                    "action_id": "button_click"
                }
            }
        ],
        text=f"Hey there <@{message['user']}>!"
    )